//
//  GameScene.swift
//  Epic Color Crash
//
//  Created by Daniel Burton on 7/25/15.
//  Copyright (c) 2015 Daniel Burton. All rights reserved.
//

import SpriteKit

class ColoredBall: SKSpriteNode {
}

class GameScene: SKScene {
    var lastBallDropped: CFTimeInterval = 0
    var newBallDelay: CFTimeInterval = 1.5
    var startBall: ColoredBall?
    var orangeAction: SKAction?
    var greenAction: SKAction?
    var purpleAction: SKAction?
    var dripSound = SKAction.playSoundFileNamed("drip.wav", waitForCompletion: false)

    func addColorBall(_ location: CGPoint, withColor color:String) {
        let sprite = ColoredBall(imageNamed: color)
        
        sprite.position = location
        sprite.name = color
        let body = SKPhysicsBody(circleOfRadius: 25)
        sprite.physicsBody = body
        body.allowsRotation = false
        body.friction = 0.0
        self.addChild(sprite)
    }
    
    func combineSprites(_ spriteA: ColoredBall, spriteB: ColoredBall, color: String) {
        spriteB.position.x = (spriteA.position.x + spriteB.position.x) / 2
        spriteB.position.y = (spriteA.position.y + spriteB.position.y) / 2
        switch color {
        case "orange":
            spriteB.run(orangeAction!)
        case "green":
            spriteB.run(greenAction!)
        case "purple":
            spriteB.run(purpleAction!)
        default:
            spriteB.texture = SKTexture(imageNamed: color)
        }
        spriteA.removeFromParent()
        spriteB.name = color
    }

    func primaryContact(_ spriteA: ColoredBall, spriteB: ColoredBall) {
        if spriteA.name == "red" {
            if spriteB.name == "yellow" {
                // make an orange ball
                combineSprites(spriteA, spriteB: spriteB, color: "orange")
            } else if spriteB.name == "blue" {
                // make a purple ball
                combineSprites(spriteA, spriteB: spriteB, color: "purple")
                
            }
        } else if spriteA.name == "yellow" {
            if spriteB.name == "red" {
                // make an orange ball
                combineSprites(spriteA, spriteB:spriteB, color: "orange")
            } else if spriteB.name == "blue" {
                // make a green ball
                combineSprites(spriteA, spriteB: spriteB, color: "green")
            }
        } else if spriteA.name == "blue" {
            if spriteB.name == "red" {
                // make a purple ball
                combineSprites(spriteA, spriteB: spriteB, color: "purple")
            } else if spriteB.name == "yellow" {
                // make a green ball
                combineSprites(spriteA, spriteB: spriteB, color: "green")
            }
        }
        
    }
    
    override func didMove(to view: SKView) {
        
        /* Setup your scene here */
        self.scaleMode = .aspectFit

        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        if orangeAction == nil {
            let orangeAtlas = SKTextureAtlas(named: "orange")
            let orangeTextures = orangeAtlas.textureNames.map { orangeAtlas.textureNamed($0) }
            let orangeAnimation = SKAction.animate(with: orangeTextures, timePerFrame: 0.075)
            orangeAction = SKAction.sequence([dripSound,orangeAnimation])
        }
        if greenAction == nil {
            let greenAtlas = SKTextureAtlas(named: "green")
            let greenTextures = greenAtlas.textureNames.map { greenAtlas.textureNamed($0) }
            let greenAnimation = SKAction.animate(with: greenTextures, timePerFrame: 0.075)
            greenAction = SKAction.sequence([dripSound,greenAnimation])
        }
        if purpleAction == nil {
            let purpleAtlas = SKTextureAtlas(named: "purple")
            let purpleTextures = purpleAtlas.textureNames.map { purpleAtlas.textureNamed($0) }
            let purpleAnimation = SKAction.animate(with: purpleTextures, timePerFrame: 0.075)
            purpleAction = SKAction.sequence([dripSound,purpleAnimation])
        }

    }
    
     func touchesBegan(_ touches: Set<NSObject>, with event: UIEvent) {
        /* Called when a touch begins */
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.location(in: self)
            let balls = self.nodes(at: location)
            if balls.count >= 1 {
                for aBall in balls {
                    startBall = aBall as? ColoredBall;
                    if startBall != nil {
                        return
                    }
                }
            }
        }
    }
     func touchesMoved(_ touches: Set<NSObject>, with event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            let location = touch.location(in: self)
            let aBall:ColoredBall? = self.atPoint(location) as? ColoredBall
            if startBall != nil && aBall != nil && aBall != startBall {
                primaryContact(startBall!, spriteB: aBall!)
                startBall = nil
                return
            }
            
        }
    }

    override func update(_ currentTime: TimeInterval) {

        // if the time has come to drop a new ball
        if currentTime > (lastBallDropped + newBallDelay) {
                lastBallDropped = currentTime
                let colors = ["red", "yellow", "blue"]
                let xpos = Int(arc4random_uniform(UInt32(self.size.width-100) ))+50
                let ypos = self.size.height - 120
                let location = CGPoint(x: xpos, y: Int(ypos))
                let colorIndex = arc4random_uniform(3)
                addColorBall(location, withColor: colors[Int(colorIndex)])
        }

    }
    

}
