//
//  Helo.swift
//  Color Crash
//
//  Created by Daniel Burton on 6/26/15.
//  Copyright (c) 2015 Daniel Burton. All rights reserved.
//

import Foundation
import iAd
import CoreMotion
import GameKit
import SpriteKit

class HelpScene: SKScene, SKPhysicsContactDelegate {
    var mainMenuDelegate: MainMenuDelegate?
    
    override func didMove(to view: SKView) {
        
        
        
        /* Setup your scene here */
        
//        self.size = view.frame.size
        let aFrame = self.frame
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: aFrame)
        self.scaleMode = .aspectFit
        self.physicsWorld.contactDelegate = self
        let body = SKPhysicsBody(circleOfRadius: 25)
        body.isDynamic = true
        body.applyImpulse(CGVector(dx: 50, dy: 50))
        body.affectedByGravity = false
        body.friction = 0.0
        body.restitution = 0.0
        self.childNode(withName: "RoamingLight")?.physicsBody = body
        self.isPaused = false
        
        /*
        let scene = GameScene.unarchiveFromFile("GameScene") as? GameScene
        if scene != nil && self.view != nil {
        // Configure the view.
        let skView = self.view! as SKView
        //            skView.showsFPS = true
        //            skView.showsNodeCount = true
        scene!.paused = true;
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        skView.ignoresSiblingOrder = true
        
        /* Set the scale mode to scale to fit the window */
        scene!.scaleMode = .AspectFill
        
        //            self.canDisplayBannerAds = true
        let bannerView = ADBannerView(adType: ADAdType.Banner)
        skView.addSubview(bannerView)
        bannerView.delegate = scene
        bannerView.frame.origin.y=skView.frame.height
        bannerView.hidden = true
        scene!.bannerHeight = bannerView.frame.height
        skView.presentScene(scene)
        var epicbanner = SKSpriteNode(imageNamed: "banner")
        epicbanner.size.width = scene!.size.width
        epicbanner.size.height = bannerView.frame.height
        epicbanner.position.x = scene!.size.width/2
        epicbanner.position.y = bannerView.frame.height/2
        
        epicbanner.name = "banner"
        scene!.addChild(epicbanner)
        }
        
        */
    }
    
    override func touchesBegan(_ touches: Set<UITouch>,
                      with event: UIEvent?) {
        /* Called when a touch begins */
        if let scene = SKScene(fileNamed: "MainMenuScene") as? MainMenuScene {
            // Configure the view.
            let skView = self.view
            //            skView.showsFPS = true
            //            skView.showsNodeCount = true
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView?.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .aspectFill
            scene.mainMenuDelegate = mainMenuDelegate
            skView?.presentScene(scene, transition: SKTransition(ciFilter: CIFilter(name: "CISwipeTransition")!, duration: 0.5))
        }
        
    }



    override func update(_ currentTime: TimeInterval) {
    }


}
