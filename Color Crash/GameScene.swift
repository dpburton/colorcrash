//
//  GameScene.swift
//  Color Crash
//
//  Created by Daniel Burton on 5/28/15.
//  Copyright (c) 2015 Daniel Burton. All rights reserved.
//

import SpriteKit
import iAd
import CoreMotion
import GameKit

class ColoredBall: SKSpriteNode {
    var removed = false
    
}

class GameScene: SKScene, SKPhysicsContactDelegate, ADBannerViewDelegate {
    var mainMenuDelegate: MainMenuDelegate?
    var ballCount = 0
    var lastBallDropped: CFTimeInterval?
    var startBall: ColoredBall?
    let primaryColored:UInt32 = 0x1 << 0
    let secondaryColored:UInt32 = 0x1 << 1
    static let hudScoreName = "Score"
    var score = 0
    var orangeBalls = Set<ColoredBall>()
    var greenBalls = Set<ColoredBall>()
    var purpleBalls = Set<ColoredBall>()
    var progressBar: SKSpriteNode?
    var levelProgress:CGFloat = 0
    var newBallDelay: CFTimeInterval = 1.5
    var level = 1
    var chainReactionBonus = 0
    var gameOver = false
    var bannerHeight:CGFloat = 0.0
    var gameOverSprite = SKSpriteNode(imageNamed: "GameOver")
    var levelLabel:SKLabelNode?
    var remainingLabel:SKLabelNode?
    var greenBombs = 0
    var orangeBombs = 0
    var purpleBombs = 0
    let greenBombTextSprite = SKLabelNode(text: "0")
    let orangeBombTextSprite = SKLabelNode(text: "0")
    let purpleBombTextSprite = SKLabelNode(text: "0")
    var dripSound = SKAction.playSoundFileNamed("drip.wav", waitForCompletion: false)
    var popSound = SKAction.playSoundFileNamed("drip.wav", waitForCompletion: false)
    var bonusSound = SKAction.playSoundFileNamed("bonus.wav", waitForCompletion: false)
    var levelUpSound = SKAction.playSoundFileNamed("levelup.wav", waitForCompletion: false)
    var gameOverSound = SKAction.playSoundFileNamed("gameover.wav", waitForCompletion: false)
    var explodeSound = SKAction.playSoundFileNamed("explode.wav", waitForCompletion: false)
    let motionManager = CMMotionManager()
    let localPlayer = GKLocalPlayer()
    let removeAction = SKAction.removeFromParent()
    var poofAtlas = SKTextureAtlas(named: "poof")
    var poofTextures: [SKTexture]!
    var poofAnimation: SKAction!
    var poofAction: SKAction!
    let orangeAtlas = SKTextureAtlas(named: "Orange")
    var orangeTextures: [SKTexture]!
    var orangeAnimation: SKAction!
    var orangeAction: SKAction!
    let greenAtlas = SKTextureAtlas(named: "Green")
    var greenTextures: [SKTexture]!
    var greenAnimation: SKAction!
    var greenAction: SKAction!
    let purpleAtlas = SKTextureAtlas(named: "Purple")
    var purpleTextures: [SKTexture]!
    var purpleAnimation: SKAction!
    var purpleAction: SKAction!
    
    func bannerViewActionShouldBegin(_ banner: ADBannerView!, willLeaveApplication willLeave: Bool) -> Bool {
        self.isPaused = true
        return true
    }
    func bannerViewActionDidFinish(_ banner: ADBannerView!) {
        self.isPaused = false
    }
    func bannerViewDidLoadAd(_ banner: ADBannerView!) {
        
        //        CGRect contentFrame = self.containerView.bounds;
        
        //        CGRect bannerFrame = self.bannerView.bounds;
        
        
        UIView.animate(withDuration: 0.25, animations: {
            let theView = self.view!
            if (banner.isBannerLoaded) {
                banner.frame.origin.y = theView.frame.height - banner.frame.height
                banner.isHidden = false
            } else {
                banner.frame.origin.y = -banner.frame.height
                banner.isHidden = true
            }
        })
        return
    }
    
    func bannerView(_ banner: ADBannerView!, didFailToReceiveAdWithError error: Error!) {
        UIView.animate(withDuration: 0.25, animations: {
            banner.frame.origin.y = -banner.frame.height
            banner.isHidden = true
        })
        
    }
    
    override func didMove(to view: SKView) {
        
        
        //        let orangeBombAtlas = SKTextureAtlas(named: "OrangeBomb")
        //        sorted(orangeBombAtlas.textureNames as! [String]).map { orangeBombAtlas.textureNamed($0) }
        
        
        /* Setup your scene here */
        
        //    let poofSprite = SKSpriteNode(imageNamed: "poof1")
        poofTextures = poofAtlas.textureNames.sorted().map { self.poofAtlas.textureNamed($0) }
        poofAnimation = SKAction.animate(with: poofTextures, timePerFrame: 0.05)
        poofAction = SKAction.sequence([poofAnimation, removeAction])
        
        if orangeAction == nil {
            orangeTextures = orangeAtlas.textureNames.sorted().map { self.orangeAtlas.textureNamed($0) }
            orangeAnimation = SKAction.animate(with: orangeTextures, timePerFrame: 0.075)
            orangeAction = SKAction.sequence([dripSound,orangeAnimation])
        }
        if greenAction == nil {
            greenTextures = greenAtlas.textureNames.sorted().map { self.greenAtlas.textureNamed($0) }
            greenAnimation = SKAction.animate(with: greenTextures, timePerFrame: 0.075)
            greenAction = SKAction.sequence([dripSound,greenAnimation])
        }
        if purpleAction == nil {
            purpleTextures = purpleAtlas.textureNames.sorted().map { self.purpleAtlas.textureNamed($0) }
            purpleAnimation = SKAction.animate(with: purpleTextures, timePerFrame: 0.075)
            purpleAction = SKAction.sequence([dripSound,purpleAnimation])
        }
        //        self.size = view.frame.size
        self.scaleMode = .aspectFit
        var aFrame = self.frame
        aFrame.size.height -= bannerHeight
        aFrame.origin.y = bannerHeight
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: aFrame)
        
        self.physicsWorld.contactDelegate = self
        
        motionManager.deviceMotionUpdateInterval = 0.025
        motionManager.startDeviceMotionUpdates()
        
        // move the score label into view
        progressBar = self.childNode(withName: "progressBar") as? SKSpriteNode
        if progressBar != nil {
            progressBar!.size.width = 5
            
        }
        levelLabel = self.childNode(withName: "Level") as? SKLabelNode
        
        remainingLabel = self.childNode(withName: "Remaining") as? SKLabelNode
        let purpleBonus = self.childNode(withName: "purpleBonus")
        if purpleBonus != nil {
            purpleBombTextSprite.position.x = purpleBonus!.position.x
            purpleBombTextSprite.position.y = purpleBonus!.position.y - 8
            purpleBombTextSprite.fontColor = UIColor(red: 1.0, green: 0.0, blue: 1.0, alpha: 1.0)
            purpleBombTextSprite.fontSize = 20
            purpleBombTextSprite.zPosition = 1
            purpleBombTextSprite.fontName = "Papyrus"
            self.addChild(purpleBombTextSprite)
            
        }
        let orangeBonus = self.childNode(withName: "orangeBonus")
        if orangeBonus != nil {
            orangeBombTextSprite.position.x = orangeBonus!.position.x
            orangeBombTextSprite.position.y = orangeBonus!.position.y - 8
            orangeBombTextSprite.fontColor = UIColor(red: 1.0, green: 0.5, blue: 0.5, alpha: 1.0)
            orangeBombTextSprite.fontSize = 20
            orangeBombTextSprite.zPosition = 1
            orangeBombTextSprite.fontName = "Papyrus"
            
            self.addChild(orangeBombTextSprite)
        }
        let greenBonus = self.childNode(withName: "greenBonus")
        if greenBonus != nil {
            greenBombTextSprite.position.x = greenBonus!.position.x
            greenBombTextSprite.position.y = greenBonus!.position.y - 8
            self.addChild(greenBombTextSprite)
            greenBombTextSprite.fontColor = UIColor(red: 0.0, green: 1.0, blue: 0.0, alpha: 1.0)
            greenBombTextSprite.fontSize = 20
            greenBombTextSprite.zPosition = 1
            greenBombTextSprite.fontName = "Papyrus"
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        /* Called when a touch begins */
        guard let uiEvent = event else {return}
        
        chainReactionBonus = 0
        for touch in touches {
            let location = touch.location(in: self)
            if gameOver {
                
                if uiEvent.timestamp > lastBallDropped! + 3 {
                    
                    if let scene = SKScene(fileNamed: "MainMenuScene") as? MainMenuScene {
                        scene.mainMenuDelegate = mainMenuDelegate
                        // Configure the view.
                        if let skView = self.view {
                            //            skView.showsFPS = true
                            //            skView.showsNodeCount = true
                            /* Sprite Kit applies additional optimizations to improve rendering performance */
                            skView.ignoresSiblingOrder = true
                            
                            /* Set the scale mode to scale to fit the window */
                            scene.scaleMode = .aspectFill
                            
                            skView.presentScene(scene, transition: SKTransition(ciFilter: CIFilter(name: "CISwipeTransition")!, duration: 0.5))
                            
                        }
                    }
                    
                    ballCount = 0
                    gameOverSprite.removeFromParent()
                    gameOver = false
                    score = 0
                    newBallDelay = 1.5
                    level = 1
                    levelLabel?.text = String.localizedStringWithFormat(NSLocalizedString("%d", comment: ""), level)
                    levelProgress = 0
                    if progressBar != nil {
                        progressBar!.size.width = 5
                        
                    }
                    orangeBombs = 0
                    greenBombs = 0
                    purpleBombs = 0
                    orangeBombTextSprite.text = String.localizedStringWithFormat(NSLocalizedString("%d", comment: ""), orangeBombs)
                    greenBombTextSprite.text = String.localizedStringWithFormat(NSLocalizedString("%d", comment: ""), greenBombs)
                    purpleBombTextSprite.text = String.localizedStringWithFormat(NSLocalizedString("%d", comment: ""), purpleBombs)
                    orangeBalls.removeAll(keepingCapacity: true)
                    greenBalls.removeAll(keepingCapacity: true)
                    purpleBalls.removeAll(keepingCapacity: true)
                }
                return
            }
            let orangeBombEmitter = self.childNode(withName: "orangeBonus")
            if orangeBombs > 0 && orangeBombEmitter != nil && orangeBombEmitter!.contains(location) {
                // drop an orange bomb
                let sprite = ColoredBall(imageNamed: "orange")
                let orangeBombAtlas = SKTextureAtlas(named: "OrangeBomb")
                let orangeBombTextures = orangeBombAtlas.textureNames.sorted().reversed().map { orangeBombAtlas.textureNamed($0) }
                let orangeBombCountdown = SKAction.animate(with: orangeBombTextures, timePerFrame: 0.0625)
                let orangeBombAction = SKAction.sequence([orangeBombCountdown, explodeSound, removeAction])
                sprite.run(orangeBombAction, completion: {
                    var eliminated = 0
                    for node in self.children {
                        let aBall = node as? ColoredBall
                        if aBall != nil && (aBall!.name == "red" || aBall!.name == "yellow") {
                            aBall!.run(self.poofAction)
                            eliminated += 1
                        }
                    }
                    
                    /*                    for ballToRemove in self.orangeBalls {
                     if ballToRemove.parent != nil {
                     ballToRemove.removeFromParent()
                     self.orangeBalls.remove(ballToRemove)
                     eliminated++
                     }
                     }
                     */
                    if self.gameOver {
                        self.animatedLabel(
                            NSLocalizedString("Late Bomb Bonus x2", comment: ""),
                            position: CGPoint(x: self.size.width/2, y: self.size.height/2),
                            color: UIColor(red: 1, green: 1, blue: 1, alpha: 1),
                            speed: 25.0)
                        self.score += eliminated
                    }
                    self.score += eliminated
                })
                
                orangeBombs -= 1
                sprite.xScale = 1
                sprite.yScale = 1
                sprite.position = location
                sprite.name = "orangeBomb"
                let body = SKPhysicsBody(circleOfRadius: 25)
                sprite.physicsBody = body
                body.allowsRotation = false
                body.friction = 0.0
                body.categoryBitMask = primaryColored
                body.collisionBitMask = primaryColored
                body.contactTestBitMask = primaryColored
                self.addChild(sprite)
                //                orangeBalls.insert(sprite)
                orangeBombTextSprite.text = String.localizedStringWithFormat(NSLocalizedString("%d", comment: ""), orangeBombs)
                
            }
            let purpleBombEmitter = self.childNode(withName: "purpleBonus")
            if purpleBombs > 0 && purpleBombEmitter != nil && purpleBombEmitter!.contains(location) {
                // drop an orange bomb
                let sprite = ColoredBall(imageNamed: "purple")
                let purpleBombAtlas = SKTextureAtlas(named: "PurpleBomb")
                let purpleBombTextures = purpleBombAtlas.textureNames.sorted().reversed().map { purpleBombAtlas.textureNamed($0) }
                let purpleBombCountdown = SKAction.animate(with: purpleBombTextures, timePerFrame: 0.0625)
                let purpleBombAction = SKAction.sequence([purpleBombCountdown, explodeSound,removeAction])
                sprite.run(purpleBombAction, completion: {
                    var eliminated = 0
                    for node in self.children {
                        let aBall = node as? ColoredBall
                        if aBall != nil && (aBall!.name == "red" || aBall!.name == "blue") {
                            aBall!.run(self.poofAction)
                            eliminated += 1
                        }
                    }
                    if self.gameOver {
                        self.animatedLabel(
                            NSLocalizedString("Late Bomb Bonus x2", comment: ""),
                            position: CGPoint(x: self.size.width/2, y: self.size.height/2),
                            color: UIColor(red: 1, green: 1, blue: 1, alpha: 1),
                            speed: 25.0)
                        self.score += eliminated
                    }
                    self.score += eliminated
                    
                    
                })
                
                purpleBombs -= 1
                sprite.xScale = 1
                sprite.yScale = 1
                sprite.position = location
                sprite.name = "purpleBomb"
                let body = SKPhysicsBody(circleOfRadius: 25)
                sprite.physicsBody = body
                body.allowsRotation = false
                body.friction = 0.0
                body.categoryBitMask = primaryColored
                body.collisionBitMask = primaryColored
                body.contactTestBitMask = primaryColored
                self.addChild(sprite)
                purpleBombTextSprite.text = String.localizedStringWithFormat(NSLocalizedString("%d", comment: ""), purpleBombs)
            }
            
            let greenBombEmitter = self.childNode(withName: "greenBonus")
            if greenBombs > 0 && greenBombEmitter != nil && greenBombEmitter!.contains(location) {
                // drop an orange bomb
                let sprite = ColoredBall(imageNamed: "green")
                let greenBombAtlas = SKTextureAtlas(named: "GreenBomb")
                let greenBombTextures = greenBombAtlas.textureNames.sorted().reversed().map { greenBombAtlas.textureNamed($0) }
                let greenBombCountdown = SKAction.animate(with: greenBombTextures, timePerFrame: 0.0625)
                let greenBombAction = SKAction.sequence([greenBombCountdown, explodeSound,removeAction])
                sprite.run(greenBombAction, completion: {
                    var eliminated = 0
                    for node in self.children {
                        let aBall = node as? ColoredBall
                        if aBall != nil && (aBall!.name == "blue" || aBall!.name == "yellow") {
                            aBall!.run(self.poofAction)
                            eliminated += 1
                        }
                    }
                    if self.gameOver {
                        self.animatedLabel(
                            NSLocalizedString("Late Bomb Bonus x2", comment: ""),
                            position: CGPoint(x: self.size.width/2, y: self.size.height/2),
                            color: UIColor(red: 1, green: 1, blue: 1, alpha: 1),
                            speed: 25.0)
                        self.score += eliminated
                    }
                    self.score += eliminated
                    
                    
                })
                
                greenBombs -= 1
                sprite.xScale = 1
                sprite.yScale = 1
                sprite.position = location
                sprite.name = "greenBomb"
                let body = SKPhysicsBody(circleOfRadius: 25)
                sprite.physicsBody = body
                body.allowsRotation = false
                body.friction = 0.0
                body.categoryBitMask = primaryColored
                body.collisionBitMask = primaryColored
                body.contactTestBitMask = primaryColored
                self.addChild(sprite)
                //                orangeBalls.insert(sprite)
                greenBombTextSprite.text = String.localizedStringWithFormat(NSLocalizedString("%d", comment: ""), greenBombs)
            }
            
            let balls = self.nodes(at: location)
            if balls.count >= 1 {
                for aBall in balls {
                    startBall = aBall as? ColoredBall;
                    //                    if startBall != nil {
                    //                        startBall!.size.height = startBall!.size.height * 1.4
                    //                    }
                }
            }
            
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            let aBall:ColoredBall? = self.atPoint(location) as? ColoredBall
            if startBall != nil && aBall != nil && aBall != startBall {
                primaryContact(startBall!, spriteB: aBall!)
                startBall = nil
                return
            }
            
        }
    }
    
    func ebcc_combineSprites(_ spriteA: ColoredBall, spriteB: ColoredBall, color: String) {
        spriteB.position.x = (spriteA.position.x + spriteB.position.x) / 2
        spriteB.position.y = (spriteA.position.y + spriteB.position.y) / 2
        switch color {
        case "orange":
            spriteB.run(orangeAction)
        case "green":
            spriteB.run(greenAction)
        case "purple":
            spriteB.run(purpleAction)
        default:
            spriteB.texture = SKTexture(imageNamed: color)
        }
        spriteA.removeFromParent()
        //        spriteA.runAction(poofAction)
        ballCount -= 1
        spriteB.name = color
    }
    func primaryContact(_ spriteA: ColoredBall, spriteB: ColoredBall) {
        if spriteA.name == "red" {
            if spriteB.name == "yellow" {
                // make an orange ball
                ebcc_combineSprites(spriteA, spriteB: spriteB, color: "orange")
                orangeBalls.insert(spriteB)
                //                spriteB.color = UIColor(red: 1, green: 0.5, blue: 0.5, alpha: 0)
            } else if spriteB.name == "blue" {
                // make a purple ball
                ebcc_combineSprites(spriteA, spriteB: spriteB, color: "purple")
                purpleBalls.insert(spriteB)
                //                spriteB.color = UIColor(red: 1, green: 0, blue: 1, alpha: 0)
                
            }
        } else if spriteA.name == "yellow" {
            if spriteB.name == "red" {
                // make an orange ball
                ebcc_combineSprites(spriteA, spriteB:spriteB, color: "orange")
                //                spriteB.color = UIColor(red: 1, green: 0.5, blue: 0.5, alpha: 0)
                orangeBalls.insert(spriteB)
            } else if spriteB.name == "blue" {
                // make a green ball
                ebcc_combineSprites(spriteA, spriteB: spriteB, color: "green")
                //                spriteB.color = UIColor(red: 0, green: 1, blue: 0, alpha: 0)
                greenBalls.insert(spriteB)
            }
        } else if spriteA.name == "blue" {
            if spriteB.name == "red" {
                // make a purple ball
                ebcc_combineSprites(spriteA, spriteB: spriteB, color: "purple")
                //                spriteB.color = UIColor(red: 1, green: 0, blue: 1, alpha: 0)
                purpleBalls.insert(spriteB)
            } else if spriteB.name == "yellow" {
                // make a green ball
                ebcc_combineSprites(spriteA, spriteB: spriteB, color: "green")
                //                spriteB.color = UIColor(red: 0, green: 1, blue: 0, alpha: 0)
                greenBalls.insert(spriteB)
            }
        }
        
    }
    
    func recurseBalls(_ aBall: ColoredBall, candidates: Set<ColoredBall>) ->Set<ColoredBall> {
        var candidates = candidates
        let contacts = aBall.physicsBody?.allContactedBodies()
        if(contacts != nil) {
            for aContact in contacts! {
                let contactBall = aContact.node as? ColoredBall
                if contactBall != nil && !contactBall!.removed {
                    if contactBall!.name == aBall.name {
                        if !candidates.contains(contactBall!) {
                            candidates.insert(contactBall!)
                            candidates = recurseBalls(contactBall!, candidates: candidates)
                            //                            recurseBalls(contactBall!, candidates: candidates)
                        }
                    }
                }
                
            }
        }
        return candidates
    }
    
    func animatedLabel(_ text:String, position:CGPoint, color:UIColor, speed:CGFloat) {
        let bonusLabel = SKLabelNode(text: text)
        bonusLabel.fontColor = color
        bonusLabel.position = position
        bonusLabel.fontName = "Papyrus"
        bonusLabel.fontSize = 16
        self.addChild(bonusLabel)
        var xSpeed:CGFloat = speed
        if position.x > self.size.width/2 {
            xSpeed = -speed
        }
        let moveNodeUp = SKAction.moveBy(x: xSpeed, y:speed*4, duration:1.0)
        let zoom = SKAction.scale(to: 2, duration: 0.25)
        let wait = SKAction.wait(forDuration: 0.5)
        let fadeAway = SKAction.fadeOut(withDuration: 0.25)
        let sequence = SKAction.sequence([moveNodeUp,bonusSound,zoom,wait,fadeAway, removeAction])
        bonusLabel.run(sequence)
        
    }
    
    func findContacts(_ aSet: inout Set<ColoredBall>, ofColor: String) {
        var candidates = Set<ColoredBall>()
        
        for aBall in aSet {
            if aBall.removed {
                continue
            }
            candidates.removeAll(keepingCapacity: true)
            candidates.insert(aBall)
            candidates = recurseBalls(aBall, candidates:candidates)
            if candidates.count > 2 {
                var eliminated = 0
                let bonusPosition = aBall.position
                for ballToRemove in candidates {
                    if !ballToRemove.removed {
                        
                        aSet.remove(ballToRemove)
                        ballToRemove.removed = true
                        ballToRemove.run(poofAction)
                        eliminated += 1
                        self.run(popSound)
                    }
                }
                chainReactionBonus += 1
                if(chainReactionBonus > 1) {
                    animatedLabel(String.localizedStringWithFormat(NSLocalizedString("Bonus x%d", comment: ""), chainReactionBonus), position: bonusPosition, color: UIColor(red: 0, green: 1, blue: 0, alpha: 0.75), speed: 25.0)
                }
                let bonus = eliminated - 3
                if gameOver {
                    animatedLabel(
                        NSLocalizedString("End of Game Bonus x2", comment: ""),
                        position: CGPoint(x: self.size.width/2, y: self.size.height/2),
                        color: UIColor(red: 1, green: 1, blue: 1, alpha: 1),
                        speed: 25.0)
                    chainReactionBonus *= 2
                }
                score = score + (eliminated * chainReactionBonus)
                score = score + (eliminated * chainReactionBonus)
                if bonus > 0 {
                    let fontColor: UIColor!
                    switch ofColor {
                    case "orange":
                        fontColor = UIColor(red: 1.0, green: 0.5, blue: 0.5, alpha: 0.75)
                        orangeBombs += bonus
                        orangeBombTextSprite.text = String.localizedStringWithFormat(NSLocalizedString("%d", comment: ""), orangeBombs)
                    case "green":
                        fontColor = UIColor(red: 0.0, green: 1.0, blue: 0.0, alpha: 0.75)
                        greenBombs += bonus
                        greenBombTextSprite.text = String.localizedStringWithFormat(NSLocalizedString("%d", comment: ""), greenBombs)
                    case "purple":
                        fontColor = UIColor(red: 1.0, green: 0.0, blue: 1.0, alpha: 0.75)
                        purpleBombs += bonus
                        purpleBombTextSprite.text = String.localizedStringWithFormat(NSLocalizedString("%d", comment: ""), purpleBombs)
                    default:
                        fontColor = UIColor(red: 1.0, green: 0.5, blue: 0.5, alpha: 0.75)
                        
                    }
                    animatedLabel(String.localizedStringWithFormat(NSLocalizedString("Bombs +%d", comment: ""), bonus), position: bonusPosition, color: fontColor, speed: 50)
                }
                ballCount -= eliminated
                levelProgress += 1
                if progressBar != nil {
                    progressBar!.size.width = self.size.width * levelProgress/10.0
                    
                }
                
            }
        }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        /* Called before each frame is rendered */
        let ballsize: Int = 50;
        let columnsSize = ballsize + 3
        let numCols = Int(self.size.width) / Int(columnsSize)
        let numRows = Int(self.size.height) / Int(columnsSize) - 2
        let maxBalls = numRows * numCols;
        
        if (isPaused) {
            return
        }
        let remainingBalls = maxBalls - self.children.count
        remainingLabel?.text = String.localizedStringWithFormat(NSLocalizedString("%d", comment: ""), remainingBalls)
        
        // if at maxBalls then game over
        if remainingBalls <= 0 {
            if gameOver == false {
                //                gameOverSprite = SKSpriteNode(imageNamed: "GameOver")
                gameOverSprite.zPosition = 1
                gameOverSprite.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
                gameOverSprite.setScale(0.01)
                let zoom = SKAction.scale(to: 0.35, duration: 0.5)
                SKAction.sequence([gameOverSound,zoom])
                gameOverSprite.run(zoom)
                self.addChild(gameOverSprite)
                remainingLabel?.run(gameOverSound)
                gameOver = true
                let gkScore = GKScore(leaderboardIdentifier: "ColorCrashHighScore", player: GKLocalPlayer.localPlayer())
                gkScore.value = Int64(score)
                let scoreArray: [GKScore] = [gkScore]
                GKScore.report(scoreArray, withCompletionHandler: nil)
//                    if error != nil {
//                        NSLog(error.localizedDescription)
 //                   }
//                    }
                
                let leaderView = GKGameCenterViewController()
                //                leaderView.gameCenterDelegate = self;
                leaderView.viewState = GKGameCenterViewControllerState.leaderboards
                
                
                
                /*
                 
                 var restartButton = SKSpriteNode(imageNamed: "restart")
                 restartButton.position = CGPoint(x: self.size.width/2, y: self.size.height/2-100)
                 restartButton.name = "restart"
                 restartButton.zPosition = 1
                 self.addChild(restartButton)
                 */
                
            }
            return
        }
        if motionManager.deviceMotion != nil {
            self.physicsWorld.gravity = CGVector(dx: CGFloat(6*motionManager.deviceMotion!.gravity.x), dy: -9.8)
        }
        if levelProgress >= 10 {
            /*            for node in self.children {
             let aBall = node as? ColoredBall
             if aBall != nil {
             aBall!.removeFromParent()
             }
             }
             ballCount = 0
             */
            newBallDelay = newBallDelay * 0.95
            if progressBar != nil {
                progressBar!.size.width = 5
                
            }
            
            levelProgress = 0
            level += 1
            animatedLabel(String.localizedStringWithFormat(NSLocalizedString("Level: %d", comment: ""), level),
                          position: CGPoint(x: self.size.width/2, y: self.size.height/2),
                          color: UIColor(red: 1, green: 1, blue: 1, alpha: 1),
                          speed: 25.0)
            levelLabel?.text = String.localizedStringWithFormat(NSLocalizedString("%d", comment: ""), level)
        }
        if !gameOver && ((lastBallDropped == nil) ||                             // if we haven't dropped any balls drop one
            ((self.children.count < 25 && currentTime > (lastBallDropped! + newBallDelay/2)) || currentTime > (lastBallDropped! + newBallDelay))) {         // or if the time has come to drop a new ball
            lastBallDropped = currentTime
            let colors = ["red", "yellow", "blue"]
            let xpos = Int(arc4random_uniform(UInt32(self.size.width-100) ))+50
            let ypos = self.size.height - 120
            let location = CGPoint(x: xpos, y: Int(ypos))
            let colorIndex = arc4random_uniform(3)
            ebcc_addColorBall(location, withColor: colors[Int(colorIndex)])
            ballCount += 1
        }
        findContacts(&greenBalls, ofColor: "green")
        findContacts(&orangeBalls, ofColor: "orange")
        findContacts(&purpleBalls, ofColor: "purple")
        
        let scoreLabel = self.childNode(withName: "Score") as! SKLabelNode
        scoreLabel.text = String.localizedStringWithFormat(NSLocalizedString("SCORE: %d", comment: ""), score)
        
    }
    
    
    func ebcc_handlePan(_ panRec: UIPanGestureRecognizer?) {
        if panRec != nil {
            switch panRec!.state {
            case .began:
                let panLoc = panRec!.location(in: self.view)
                let balls = self.nodes(at: self.convertPoint(fromView: panLoc))
                
                
                if balls.count >= 1 {
                    for aBall in balls {
                        startBall = aBall as? ColoredBall;
                        if startBall != nil {
                            startBall!.size.height = startBall!.size.height * 1.4
                        }
                    }
                }
                
            case .ended:
                let panLoc = panRec!.location(in: self.view)
                let aBall:ColoredBall? = self.atPoint(self.convertPoint(fromView: panLoc)) as? ColoredBall
                if startBall != nil && aBall != nil && aBall != startBall {
                    primaryContact(startBall!, spriteB: aBall!)
                }
            case .changed:
                let panLoc = panRec!.location(in: self.view)
                let aBall:ColoredBall? = self.atPoint(self.convertPoint(fromView: panLoc)) as? ColoredBall
                if startBall != nil && aBall != nil && aBall != startBall {
                    primaryContact(startBall!, spriteB: aBall!)
                }
                panRec?.isEnabled=false
                panRec?.isEnabled=true
            default:
                break;
            }
            
            
        }
        
    }
    
    func ebcc_addColorBall(_ location: CGPoint, withColor color:String) {
        let sprite = ColoredBall(imageNamed: color)
        
        sprite.xScale = 1
        sprite.yScale = 1
        sprite.position = location
        sprite.name = color
        let body = SKPhysicsBody(circleOfRadius: 25)
        sprite.physicsBody = body
        body.allowsRotation = false
        body.friction = 0.0
        body.categoryBitMask = primaryColored
        body.collisionBitMask = primaryColored
        body.contactTestBitMask = primaryColored
        self.addChild(sprite)
    }
}
