//
//  MainMenuScene.swift
//  Color Crash
//
//  Created by Daniel Burton on 6/19/15.
//  Copyright (c) 2015 Daniel Burton. All rights reserved.
//

import SpriteKit
import iAd
import CoreMotion
import GameKit

protocol MainMenuDelegate {
    func displayHighScores()
}

class MainMenuScene: SKScene, SKPhysicsContactDelegate {
    var bannerHeight:CGFloat = 0.0
    var mainMenuDelegate: MainMenuDelegate?
    
    
    override func didMove(to view: SKView) {
        /* Setup your scene here */
        
//        self.size = view.frame.size
        let aFrame = self.frame
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: aFrame)
        self.scaleMode = .aspectFit

        self.physicsWorld.contactDelegate = self
        let body = SKPhysicsBody(circleOfRadius: 25)

        body.isDynamic = true
        body.applyImpulse(CGVector(dx: 50, dy: 50))
        body.affectedByGravity = false
        body.friction = 0.0
        body.restitution = 0.0
        self.childNode(withName: "RoamingLight")?.physicsBody = body
        self.isPaused = false
        
        /*
        let scene = GameScene.unarchiveFromFile("GameScene") as? GameScene
        if scene != nil && self.view != nil {
            // Configure the view.
            let skView = self.view! as SKView
            //            skView.showsFPS = true
            //            skView.showsNodeCount = true
            scene!.paused = true;
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene!.scaleMode = .AspectFill
            
            //            self.canDisplayBannerAds = true
            let bannerView = ADBannerView(adType: ADAdType.Banner)
            skView.addSubview(bannerView)
            bannerView.delegate = scene
            bannerView.frame.origin.y=skView.frame.height
            bannerView.hidden = true
            scene!.bannerHeight = bannerView.frame.height
            skView.presentScene(scene)
            var epicbanner = SKSpriteNode(imageNamed: "banner")
            epicbanner.size.width = scene!.size.width
            epicbanner.size.height = bannerView.frame.height
            epicbanner.position.x = scene!.size.width/2
            epicbanner.position.y = bannerView.frame.height/2
            
            epicbanner.name = "banner"
            scene!.addChild(epicbanner)
        }

        */
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        /* Called when a touch begins */
        for touch in touches {
            let location = touch.location(in: self)
            let button = self.atPoint(location)
            if button.name == "start" {
                if let scene = SKScene(fileNamed: "GameScene") as? GameScene,
                    let skView = view {
                    // Configure the view.
                    scene.mainMenuDelegate = self.mainMenuDelegate
                    //            skView.showsFPS = true
                    //            skView.showsNodeCount = true
                    scene.isPaused = true;
                    /* Sprite Kit applies additional optimizations to improve rendering performance */
                    skView.ignoresSiblingOrder = true
                    /* Set the scale mode to scale to fit the window */
                    scene.scaleMode = .aspectFit
                    
                    //            self.canDisplayBannerAds = true
                    let bannerView = ADBannerView(adType: ADAdType.banner)
                    skView.addSubview(bannerView!)
//                    let sceneFrame = scene.frame
//                    let scale = sceneFrame.height / skView.frame.height
                    skView.presentScene(scene, transition: SKTransition(ciFilter: CIFilter(name: "CISwipeTransition")!, duration: 0.5))
                }

            }
            if button.name == "scores" {
                self.mainMenuDelegate?.displayHighScores()
            }
            if button.name == "help" {
                if let scene = SKScene(fileNamed: "HelpScene") as? HelpScene,
                    let skView = view {
                    scene.mainMenuDelegate = mainMenuDelegate
                    // Configure the view.
                    //            skView.showsFPS = true
                    //            skView.showsNodeCount = true
                    scene.isPaused = true;
                    /* Sprite Kit applies additional optimizations to improve rendering performance */
                    skView.ignoresSiblingOrder = true
                    
                    /* Set the scale mode to scale to fit the window */
//                    scene!.scaleMode = .ResizeFill
                    
                    //            self.canDisplayBannerAds = true
                    skView.presentScene(scene, transition: SKTransition(ciFilter: CIFilter(name: "CISwipeTransition")!, duration: 0.5))
                    
                }

            }
            if button.name == "epicbiking" {
                UIApplication.shared.openURL(URL(string: "http://www.epicbiking.com")!)
            }
            
        }

    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        let body = self.childNode(withName: "RoamingLight")?.physicsBody
        if body != nil {
            
            if (body!.velocity.dx <= 0) {
                if (body!.velocity.dx > -35) {
                body!.applyImpulse(CGVector(dx: -15, dy: 0))
                }
            }
            if (body!.velocity.dx >= 0) {
                if (body!.velocity.dx < 32) {
                    body!.applyImpulse(CGVector(dx: 18, dy: 0))
                }
            }
            if (body!.velocity.dy <= 0) {
                if (body!.velocity.dy > -31) {
                    body!.applyImpulse(CGVector(dx: 0, dy: -20))
                }
            }
            if (body!.velocity.dy >= 0) {
                if (body!.velocity.dy < 34) {
                    body!.applyImpulse(CGVector(dx: -0, dy: 17))
                }
            }

            
        }
        
    }
    
    
}
